#ifndef TASKLIST_H
#define TASKLIST_H

#include <QWidget>
#include <QListView>
#include <QStringList>
#include <QStringListModel>
#include <QBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>

class TaskList : public QWidget
{
    Q_OBJECT
public:
    explicit TaskList(QWidget *parent = nullptr);
private:
    QAbstractItemModel *taskList;
    QLineEdit *input;
    QPushButton *addTaskButton;
    QListView *listView;
public slots:
    void addTask();

signals:


};

#endif // TASKLIST_H
