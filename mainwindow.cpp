#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    calendar = new QCalendarWidget(this);
//    calendar->setGridVisible(true);

    this->setCentralWidget(calendar);


    QDockWidget *dockWidget = new QDockWidget(("Task List"), this);
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea);

    taskList = new TaskList(dockWidget);

    dockWidget->setWidget(taskList);
    this->addDockWidget(Qt::LeftDockWidgetArea, dockWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

