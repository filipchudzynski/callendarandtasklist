#include "tasklist.h"
#include <QDebug>
TaskList::TaskList(QWidget *parent) : QWidget(parent)
{
    //QWidget *taskListWidget = new QWidget(parent);
    QLayout *taskListLayout = new QVBoxLayout(this);

    QStringList numbers;
    numbers << "One" << "Two" << "Three" << "Four" << "Five";

    //Task list widget
    taskList = new QStringListModel(numbers,this);

    //List of tasks
    listView = new QListView(this);
    listView->setModel(taskList);

    //New task form
    QWidget *newTaskForm = new QWidget(parent);
    QFormLayout *formLayout = new QFormLayout(newTaskForm);
    input = new QLineEdit(newTaskForm);
    formLayout->addRow("&Title:", input);
    newTaskForm->setLayout(formLayout);

    //Add new task button
    addTaskButton = new QPushButton("Add task",this);
    connect(addTaskButton,&QPushButton::released,this,&TaskList::addTask);



    taskListLayout->addWidget(listView);
    taskListLayout->addWidget(newTaskForm);
    taskListLayout->addWidget(addTaskButton);

    this->setLayout(taskListLayout);

}

void TaskList::addTask(){
    QString taskName = input->text();
    input->clear();
    taskList->insertRows(taskList->rowCount(),1);
    QModelIndex index = taskList->index( taskList->rowCount()-1,0);
    taskList->setData(index,taskName);
    listView->update();

//    qDebug()<<taskName;
}
